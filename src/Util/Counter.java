package Util;

import static java.lang.System.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author xenonpk
 */
public class Counter {

    private int value;

    /**
     *Inicia o contador com o valor especificado em startv
     * @param startv Valor usado para inicializar o contador
     */
    public Counter(int startv) {
        value = startv;
    }

    /**
     *Reinicia o contador a 0
     */
    public void resetCounter() {
        value = 0;
    }

    /**
     *Devolve o valor atual do contador
     * @return
     */
    public int getValue() {
        return (value);
    }

    /**
     *Incrementa o contador um valor
     */
    public void increment() {
        value++;
    }

    /**
     *Incrementa o contador
     * @param val Valor usado para o incremento
     */
    public void increment(int val) {
        value += val;
    }

    /**
     *Decrementa o contador se o valor resultante for positivo
     */
    public void decrement() {
        if (value != 0) {
            value--;
        }
    }

    /**
     *Decrementa o contador pelo valor definido em val se o valor resultante for positivo
     * @param val Valor usado para o decremento
     */
    public void decrement(int val) {
        if (value < val) {
            resetCounter();
        } else {
            value -= val;
        }
    }

    /**
     *Cria um novo objeto igual ao objeto atual
     * @return
     */
    @Override
    public Counter clone() {
        return (new Counter(value));
    }

    /**
     *Testa se o objeto a é ou não igual ao objeto atual
     * @param a Objeto a testar 
     * @return
     */
    @Override
    public boolean equals(Object a) {
        Counter b = (Counter) a;
        if (b == null || getClass() != b.getClass()) {
            return false;
        }
        return value == b.getValue();
    }

    /**
     *"Converte" o objeto atual em string
     * @return
     */
    @Override
    public String toString() {
        String v = "[" + value + "]";
        return (v);
    }
}

