package Principal;

import Util.Counter;
import static java.lang.System.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author xenonpk
 */
public class main {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        Counter a = new Counter(10);
        a.increment();
        print(a);
        a.increment(20);
        print(a);
        a.resetCounter();
        print(a);
        a.decrement(20);
        print(a);
        a.increment(45);
        print(a);
        a.decrement(10);
        print(a);
        Counter b = a.clone();
        System.out.println(a.equals(b));
        print(b);
        a.increment(10);
        b.decrement(20);
        print(b);
        print(a);
        System.out.println(a.equals(b));
    }

    private static void print(Counter a) {
        System.out.println(a.getValue());
        System.out.println(a.toString());
    }
}
